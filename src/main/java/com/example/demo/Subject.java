package com.example.demo;

public class Subject {
    private int sid;
    private String sname;
    private String stech;

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getStech() {
        return stech;
    }

    public void setStech(String stech) {
        this.stech = stech;
    }

    @Override
    public String toString() {
        return "Subject{" +
                "sid=" + sid +
                ", sname='" + sname + '\'' +
                ", stech='" + stech + '\'' +
                '}';
    }
}
